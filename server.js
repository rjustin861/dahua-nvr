const path = require("path");
const express = require("express");
const cors = require("cors");
const eventemitter3 = require("eventemitter3");
const port = process.env.PORT || 3000;

const app = express();
const emitter = new eventemitter3();

app.use(cors());
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, "/public")));

require("./middleware.js")(emitter);
require("./routes.js")(app, emitter);

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
