const fs = require("fs");
const util = require("util");

require("dotenv").config();

const log_file = fs.createWriteStream(__dirname + "/debug.log", { flags: "w" });
const log_stdout = process.stdout;
const options = {
  hostname: process.env.DAHUA_HOST,
  port: process.env.DAHUA_PORT,
  username: process.env.DAHUA_USER,
  password: process.env.DAHUA_PASSWORD,
  log_level: parseInt(process.env.LOG_LEVEL),
  fr_channel: 1
};

module.exports.log = function(d) {
  log_file.write(util.format(d) + "\n");
  log_stdout.write(util.format(d) + "\n");
};

module.exports.options = options;
