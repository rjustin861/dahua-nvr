module.exports = function(app, emitter) {
  app.get("/api/system", (req, res, next) => {
    res.send("Hello World");
  });

  app.get("/api/alert", (req, res, next) => {
    res.writeHead(200, {
      "Content-Type": "text/event-stream",
      "Cache-Control": "no-cache",
      Connection: "keep-alive"
    });

    const heartbeat = setInterval(() => {
      res.write("\n");
    }, 5000);

    const onDetect = data => {
      res.write("retry: 500\n");
      res.write("event: detectFace\n");
      res.write(`data: ${data}\n\n`);
    };

    emitter.on("face", onDetect);

    req.on("close", () => {
      clearInterval(heartbeat);
      emitter.removeListener("face", onDetect);
    });
  });
};
