if (!!window.EventSource) {
  source = new EventSource("/api/alert");
} else {
  throw new Error("No EventSource API support");
}

const fr_img = document.querySelector(".img_fr");
const fr_staff = document.querySelector(".personnel_wrap");
const fr_sex = document.querySelector("#fr_sex");
const fr_age = document.querySelector("#fr_age");
const fr_mask = document.querySelector("#fr_mask");
const fr_eye = document.querySelector("#fr_eye");
const fr_mouth = document.querySelector("#fr_mouth");
const fr_beard = document.querySelector("#fr_beard");

source.addEventListener(
  "detectFace",
  function(res) {
    console.log(res);

    const {
      filename,
      GroupName,
      Sex,
      Age,
      Mask,
      Eye,
      Mouth,
      Beard
    } = JSON.parse(res.data);

    setTimeout(() => {
      fr_img.src = filename;
      if (GroupName == undefined) {
        fr_staff.textContent = "Unknown";
        fr_staff.classList.remove(...fr_staff.classList);
        fr_staff.classList.add("personnel_wrap");
        fr_staff.classList.add("unknown");
      } else if (GroupName == "blacklist") {
        fr_staff.textContent = "Unknown (Blacklist)";
        fr_staff.classList.remove(...fr_staff.classList);
        fr_staff.classList.add("personnel_wrap");
        fr_staff.classList.add("blacklist");
      } else {
        fr_staff.textContent = `Personnel ${GroupName}`;
        fr_staff.classList.remove(...fr_staff.classList);
        fr_staff.classList.add("personnel_wrap");
        fr_staff.classList.add("staff");
      }
      fr_sex.textContent = Sex;
      fr_age.textContent = Age;
      fr_mask.textContent = Mask;
      fr_eye.textContent = Eye;
      fr_mouth.textContent = Mouth;
      fr_beard.textContent = Beard;
    }, 3000);
  },
  false
);
