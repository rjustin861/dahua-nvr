const Dahua = require("./Dahua");

module.exports = function(emitter) {
  Dahua.on("detectFace", function(data) {
    console.log("detectFace Event Detected : " + JSON.stringify(data));
    if (data.status !== "Unable To Detect")
      emitter.emit("face", JSON.stringify(data));
  });
};
