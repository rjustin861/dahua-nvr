const fs = require("fs");
const EventEmitter = require("events");
const request = require("request-promise");
const setKeypath = require("keypather/set");
const dayjs = require("dayjs");

const { options, log } = require("./setup.js");

class Dahua extends EventEmitter {
  constructor() {
    super();
    this.BASEURI = "http://" + options.hostname + ":" + options.port;
    this.USERNAME = options.username;
    this.PASSWORD = options.password;
    this.TRACE = !!options.log_level;
    this.INTRUSION_CHANNEL = options.intrusion_channel;
    this.FR_CHANNEL = options.fr_channel;
    this.VEHICLE_CHANNEL = options.vehicle_channel;
    this.connect();
  }

  connect() {
    //if (this.INTRUSION_CHANNEL) this.startLineDetection();
    if (this.FR_CHANNEL) this.startFaceDetection();
    //if (this.VEHICLE_CHANNEL) this.startVehicleDetection();
  }

  async startFaceDetection() {
    const opt = {
      url:
        this.BASEURI +
        `/cgi-bin/snapManager.cgi?action=attachFileProc&Flags[0]=Event&Events=[FaceRecognition]&channel=${this.FR_CHANNEL}&heartbeat=60`,
      forever: true,
      headers: { Accept: "multipart/x-mixed-replace" }
    };

    if (this.TRACE) log("Initiate detectFace API. URL:" + opt.url);

    let items = {};

    await request(opt)
      .auth(this.USERNAME, this.PASSWORD, false)
      .on("response", response => {
        if (this.TRACE) log("Response detecting face : " + response);
      })
      .on("error", err => {
        log("Error detecting face : " + err);
        this.emit("detectFace", { status: "Unable To Detect" });
      })
      .on("data", async data => {
        const body = data.toString().split("\r\n");

        // parsing items
        for (let i = 0; i < body.length; i++) {
          let item = body[i];
          //if (this.TRACE) log(item);
          if (
            item.startsWith("Events[0].Face") ||
            item.startsWith("Events[0].Candidates") ||
            item.startsWith("Events[0].Object.Image.FilePath")
          ) {
            if (
              item.startsWith(
                "Events[0].Face.Candidates[0].Person.Image[0].FilePath"
              ) ||
              item.startsWith(
                "Events[0].Candidates[0].Person.Image[0].FilePath"
              )
            )
              continue;

            const propertyAndValue = item.split("=");
            const attribute = propertyAndValue[0]
              .split(".")
              .pop()
              .replace(/[[\]]/g, "");
            const value = propertyAndValue[1];
            setKeypath(items, attribute, value);
          }
        }
        if (this.TRACE) log(items);

        if (
          Object.keys(items).length !== 0 &&
          items.hasOwnProperty("FilePath")
        ) {
          if (this.TRACE)
            log(this.BASEURI + "/cgi-bin/RPC_Loadfile" + items.FilePath);

          const outputFile = this.generateFilename(
            this.FR_CHANNEL,
            dayjs(),
            "jpg"
          );
          items.filename = outputFile;
          await this.downloadFile(items.FilePath, outputFile);
          if (this.TRACE) log(items);
          this.emit("detectFace", items);
          items = {};
        } else {
          if (this.TRACE) log("No face detected. Only heartbeat");
          this.emit("detectFace", { status: "Unable To Detect" });
        }
      })
      .on("end", function() {
        if (this.TRACE) log("Ending detecting face");
      })
      .on("close", function() {
        if (this.TRACE) log("Closing detecting face");
        this.emit("detectFace", { status: "Unable To Detect" });
      });
  }

  async downloadFile(inputFile, outputFile) {
    await request(this.BASEURI + "/cgi-bin/RPC_Loadfile" + inputFile)
      .auth(this.USERNAME, this.PASSWORD, false)
      .on("response", response => {
        if (response.statusCode !== 200) {
          this.emit("error", "ERROR ON LOAD FILE COMMAND");
        }
      })
      .on("error", error => {
        if (error.code == "ECONNRESET") {
          this.emit("error", "ERROR ON LOAD FILE COMMAND - FILE NOT FOUND?");
        } else {
          this.emit("error", "ERROR ON LOAD FILE COMMAND");
        }
      })
      .on("end", () => {
        this.emit("saveFile", {
          status: "DONE"
        });
      })
      .pipe(fs.createWriteStream("public/" + outputFile));
  }

  generateFilename(channel, datetime, filetype) {
    let filename = "ch" + channel + "_";

    const startDate = dayjs(datetime, "YYYYMMDDhhmmss");

    filename += startDate.format("YYYYMMDDhhmmss");
    filename += "." + filetype;

    return filename;
  }
}

Object.freeze(Dahua);
module.exports = new Dahua();
